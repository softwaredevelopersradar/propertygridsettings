﻿using PropertyGridSettings.Model;
using System;
using System.Globalization;
using System.Windows.Data;

namespace PropertyGridSettings.Converters
{
    public class AccessTypesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (AccessTypes)System.Convert.ToByte(value);
        }
    }
}
