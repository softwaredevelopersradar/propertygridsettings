﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PropertyGridSettings.Properties {
    using System;
    
    
    /// <summary>
    ///   Класс ресурса со строгой типизацией для поиска локализованных строк и т.д.
    /// </summary>
    // Этот класс создан автоматически классом StronglyTypedResourceBuilder
    // с помощью такого средства, как ResGen или Visual Studio.
    // Чтобы добавить или удалить член, измените файл .ResX и снова запустите ResGen
    // с параметром /str или перестройте свой проект VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Возвращает кэшированный экземпляр ResourceManager, использованный этим классом.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("PropertyGridSettings.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Перезаписывает свойство CurrentUICulture текущего потока для всех
        ///   обращений к ресурсу с помощью этого класса ресурса со строгой типизацией.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot; ?&gt;
        ///
        ///&lt;Translation&gt;
        ///
        ///
        ///  &lt;Translate ID=&quot;IpAddress&quot;&gt;
        ///    &lt;RU&gt;IP адрес&lt;/RU&gt;
        ///    &lt;EN&gt;IP address&lt;/EN&gt;
        ///    &lt;AZ&gt;IP addres&lt;/AZ&gt;
        ///  &lt;/Translate&gt;
        ///
        ///  &lt;Translate ID=&quot;Port&quot;&gt;
        ///    &lt;RU&gt;Порт&lt;/RU&gt;
        ///    &lt;EN&gt;Port&lt;/EN&gt;
        ///    &lt;AZ&gt;Port&lt;/AZ&gt;
        ///  &lt;/Translate&gt;
        ///
        ///  &lt;Translate ID=&quot;Language&quot;&gt;
        ///    &lt;RU&gt;Язык&lt;/RU&gt;
        ///    &lt;EN&gt;Language&lt;/EN&gt;
        ///    &lt;AZ&gt;Dil&lt;/AZ&gt;
        ///  &lt;/Translate&gt;
        ///
        ///  &lt;Translate ID=&quot;Num&quot;&gt;
        ///    &lt;RU&gt;Номер&lt;/RU&gt;
        ///    &lt;EN&gt;Num&lt;/EN&gt;
        ///    &lt;AZ&gt;Otaq&lt;/AZ&gt;
        ///  &lt;/Translate&gt;
        ///
        ///  &lt;Translate ID=&quot;Latitud [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string Translation {
            get {
                return ResourceManager.GetString("Translation", resourceCulture);
            }
        }
    }
}
