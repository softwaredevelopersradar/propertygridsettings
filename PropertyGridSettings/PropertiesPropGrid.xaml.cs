﻿using PropertyGridSettings.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using WpfPasswordControlLibrary;

namespace PropertyGridSettings
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class PropertiesPropGrid : UserControl, INotifyPropertyChanged
    {

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Events
        public event EventHandler<LocalProperties> OnLocalPropertiesChanged = (sender, newModel) => { };
        public event EventHandler OnLocalDefaultButtonClick = (sender, obj) => { };
        public event EventHandler<LocalProperties> OnApplyButtonClick = (sender, obj) => { };
        public event EventHandler<LocalProperties> OnDeleteButtonClick = (sender, obj) => { };
        public event EventHandler<LocalProperties> OnDefaultButtonClick = (sender, obj) => { };
        public event EventHandler<Languages> OnLanguageChanged = (sender, language) => { };
        public event EventHandler<string> OnMapTilesPathChanged = (sender, folderMapTiles) => { };
        public event EventHandler<string> OnMapPathChanged = (sender, folderMapTiles) => { };
        public event EventHandler<ViewCoord> OnCoordinateFormatChanged = (sender, coordinateView) => { };
        public event EventHandler<string> OnComPortChanged = (sender, comPort) => { };
        public event EventHandler<string> OnComPortGSMChanged = (sender, comPort) => { };
        public event EventHandler<float> OnCourseOEMAngleChanged = (sender, courseAngle) => { };
        public event EventHandler<float> OnCourseBRDAngleChanged = (sender, courseAngle) => { };
        public event EventHandler<bool> OnOEMAvailability = (sender, availability) => { };
        public event EventHandler<bool> OnBRDAvailability = (sender, availability) => { };
        public event EventHandler<bool> OnPasswordChecked = (sender, isCorrect) => { };
        #endregion

        #region Properties

        private readonly WindowPass _accessWindowPass = new WindowPass();

        private bool isLocalSelected;
        public bool IsLocalSelected
        {
            get => isLocalSelected;
            set
            {
                if (isLocalSelected == value) return;
                isLocalSelected = value;
                OnPropertyChanged();
            }
        }

        private bool isValid;
        public bool IsValid
        {
            get => isValid;
            private set
            {
                if (isValid == value) return;
                isValid = value;
                OnPropertyChanged();
            }
        }

        private string errorMessage;
        public string ErrorMessage
        {
            get => errorMessage;
            set
            {
                if (errorMessage == value) return;
                errorMessage = value;
                OnPropertyChanged();
            }
        }

        private bool _showToolTip;
        public bool ShowToolTip
        {
            get => _showToolTip;
            set
            {
                if (_showToolTip == value) return;
                _showToolTip = value;
                OnPropertyChanged();
            }
        }

        private LocalProperties savedLocal;

        public LocalProperties Local
        {
            get => (LocalProperties)Resources["localProperties"];
            set
            {
                if ((LocalProperties)Resources["localProperties"] == value)
                {
                    savedLocal = value.Clone();
                    return;
                }
                ((LocalProperties)Resources["localProperties"]).Update(value);
                savedLocal = value.Clone();
            }
        }
        #endregion

        #region Language
        Dictionary<string, string> TranslateDic;

        private void ChangeLanguage(Languages newLanguage)
        {
            SetDynamicResources(newLanguage);
            LoadDictionary();
            SetCategoryLocalNames();
        }
        private void SetDynamicResources(Languages newLanguage)
        {
            try
            {
                ResourceDictionary dict = new ResourceDictionary();
                switch (newLanguage)
                {
                    case Languages.EN:
                        dict.Source = new Uri("./PropertyGridSettings;component/Language/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.RU:
                        dict.Source = new Uri("./PropertyGridSettings;component/Language/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    case Languages.AZ:
                        dict.Source = new Uri("./PropertyGridSettings;component/Language/StringResource.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("./PropertyGridSettings;component/Language/StringResource.EN.xaml",
                                          UriKind.Relative);
                        break;
                }
                Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            {
                //TODO
            }
        }

        void LoadDictionary()
        {
            var translation = Properties.Resources.Translation;
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(translation);
            TranslateDic = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == Local.Common.Language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }
                        }
                    }
                }
            }
        }

        private void SetCategoryLocalNames()
        {
            try
            {
                foreach (var nameCategory in PropertyLocal.Categories.Select(category => category.Name).ToList())
                {
                    PropertyLocal.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region Validation

        private void Validate()
        {
            bool isvalid = true;
            string error = "";
            Action<object> funcValidation = (object obj) =>
            {
                foreach (var property in obj.GetType().GetProperties())
                {
                    foreach (var subProperty in property.PropertyType.GetProperties())
                    {
                        try
                        {
                            if (!(property.Name == "DependencyObjectType" || property.Name == "Dispatcher" || property.Name == "BRD"))
                            {
                                if (!string.IsNullOrWhiteSpace(Convert.ToString(((IDataErrorInfo)property.GetValue(obj))[subProperty.Name])))
                                {
                                    error += $"{TranslateDic[property.Name]}: {TranslateDic[subProperty.Name]}" + "\n";
                                    isvalid = false;
                                }
                            }
                        }
                        catch
                        {
                            //  Console.WriteLine(property.Name+ "  " + subProperty.Name);
                            continue;
                        }

                    }
                }
                IsValid = isvalid;
                ShowToolTip = !isvalid;
                ErrorMessage = error;
                return;
            };

            funcValidation(Local);

        }

        #endregion

        #region Access
        private bool _isAccessChecked = false;

        private void InitAccessWindow()
        {
            _accessWindowPass.OnEnterInvalidPassword += _accessWindowPass_OnEnterInvalidPassword;
            _accessWindowPass.OnEnterValidPassword += _accessWindowPass_OnEnterValidPassword;
            _accessWindowPass.OnClosePasswordBox += _accessWindowPass_OnClosePasswordBox;
            _accessWindowPass.Resources = Resources;
            _accessWindowPass.SetResourceReference(WindowPass.LablePasswordProperty, "labelPassword");
        }

        private void _accessWindowPass_OnEnterValidPassword(object sender, EventArgs e)
        {
            _isAccessChecked = true;
            Local.Common.Access = AccessTypes.Admin;
            ChangeVisibilityLocalProperties(true);
            _accessWindowPass.Hide();
            OnPasswordChecked(this, true);
        }



        private void ChangeVisibilityLocalProperties(bool browsable)
        {
            PropertyLocal.Properties[nameof(LocalProperties.BRD)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.OEM)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.Map)].IsBrowsable = browsable;
        }

        private void _accessWindowPass_OnClosePasswordBox(object sender, EventArgs e)
        {
            Local.Common.Access = AccessTypes.User;
        }

        private void _accessWindowPass_OnEnterInvalidPassword(object sender, EventArgs e)
        {
            Local.Common.Access = AccessTypes.User;
            _accessWindowPass.Hide();

            OnPasswordChecked(this, false);
        }

        private void CheckLocalAccess()
        {
            if (!_isAccessChecked)
            {
                Task.Run(() =>
                 Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                 {
                     Local.Common.Access = AccessTypes.User;
                     _accessWindowPass.ShowDialog();
                 }));
            }
            else
                _isAccessChecked = false;
        }

        public void SetPassword(string password)
        {
            _accessWindowPass.ValidPassword = password;
        }

        private void SetBindingAccess()
        {
            Binding binding = new Binding();
            binding.Source = Local.Common;
            binding.Path = new PropertyPath(nameof(Local.Common.Access));
            binding.Mode = BindingMode.TwoWay;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
        }

        #endregion

        public PropertiesPropGrid()
        {
            InitializeComponent();
            InitLocalProperties();
            InitAccessWindow();
            ChangeLanguage(savedLocal.Common.Language);
        }

        private void InitLocalProperties()
        {
            SetBindingAccess();
            savedLocal = Local.Clone();
            Local.Common.PropertyChanged += Local_PropertyChanged;
            Local.OEM.PropertyChanged += Map_PropertyChanged;
            Local.BRD.PropertyChanged += BRD_PropertyChanged;
            savedLocal.Common.PropertyChanged += SavedLocal_PropertyChanged;
            Local.OnPropertyChanged += OnPropertyChanged;

            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Common), typeof(LocalProperties), typeof(Common)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.BRD), typeof(LocalProperties), typeof(BRDProperties)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.OEM), typeof(LocalProperties), typeof(OEMProperties)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Map), typeof(LocalProperties), typeof(MapProperties)));
        }


        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Validate();
        }


        private void BRD_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(BRDProperties.CourseAngle)))
                OnCourseBRDAngleChanged(sender, Local.BRD.CourseAngle);
            if (e.PropertyName.Equals(nameof(BRDProperties.Availability)))
                OnBRDAvailability(sender, Local.BRD.Availability);
        }

        private void Map_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(OEMProperties.CourseAngle)))
                OnCourseOEMAngleChanged(sender, Local.OEM.CourseAngle);
            if (e.PropertyName.Equals(nameof(OEMProperties.Availability)))
                OnOEMAvailability(sender, Local.OEM.Availability);
        }

        private void SavedLocal_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Common.Language):
                    OnLanguageChanged(sender, savedLocal.Common.Language);
                    ChangeLanguage(savedLocal.Common.Language);
                    break;

                case nameof(Common.CoordinateView):
                    OnCoordinateFormatChanged(sender, savedLocal.Common.CoordinateView);
                    break;
                case nameof(Common.ComPort_Radio):
                    OnComPortChanged(sender, savedLocal.Common.ComPort_Radio);
                    break;
                case nameof(Common.ComPort_GSM):
                    OnComPortGSMChanged(sender, savedLocal.Common.ComPort_GSM);
                    break;
                case nameof(Common.Access):
                    if (Local.Common.Access == AccessTypes.Admin)
                    {
                        CheckLocalAccess();
                    }
                    else
                    {
                        ChangeVisibilityLocalProperties(false);
                    }
                    break;
                default:
                    break;
            }
        }

        private void Local_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            switch (e.PropertyName)
            {
                case nameof(Common.Language):
                    ChangeLanguage(Local.Common.Language);
                    OnLanguageChanged(sender, Local.Common.Language);
                    break;
                case nameof(Common.CoordinateView):
                    OnCoordinateFormatChanged(sender, Local.Common.CoordinateView);
                    break;
                case nameof(Common.ComPort_Radio):
                    OnComPortChanged(sender, Local.Common.ComPort_Radio);
                    break;
                case nameof(Common.ComPort_GSM):
                    OnComPortGSMChanged(sender, Local.Common.ComPort_GSM);
                    break;
                case nameof(Common.Access):
                    if (Local.Common.Access == AccessTypes.Admin)
                    {
                        CheckLocalAccess();
                    }
                    else
                    {
                        ChangeVisibilityLocalProperties(false);
                    }
                    break;
                default:
                    break;
            }
        }

        private void ApplyClick(object sender, RoutedEventArgs e)
        {
            OnApplyButtonClick(this, Local);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ChangeVisibilityLocalProperties(false);
        }

        private void butDefault_Click(object sender, RoutedEventArgs e)
        {
            OnDefaultButtonClick(this, Local);
        }

        private void Grid_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset-e.Delta/3);
        }
    }
}
