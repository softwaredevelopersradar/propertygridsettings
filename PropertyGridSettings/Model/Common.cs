﻿using PropertyGridSettings.Ext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
  
namespace PropertyGridSettings.Model
{
    public class Common : AbstractBaseProperties<Common>
    {
        private AccessTypes access = AccessTypes.User;
        private Languages language;
        private ViewCoord coordinateView = ViewCoord.DMSs;
        private string _comPort_Radio = "";
        private string _comPort_GSM = "";
        private int _cyclePool_Minutes = 20;
        private bool en = true;
        private bool ru = true;
        private bool az = true;

        public Common() { }

        [NotifyParentProperty(true)]
        public AccessTypes Access
        {
            get => access;
            set
            {
                if (access == value) return;
                access = value;
                Console.WriteLine($"Accsess: {access}");
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public Languages Language
        {
            get => language;
            set
            {
                if (language == value) return;
                language = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public ViewCoord CoordinateView
        {
            get => coordinateView;
            set
            {
                if (value == coordinateView) return;             
                coordinateView = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string ComPort_Radio
        {
            get => _comPort_Radio;
            set
            {
                if (value == _comPort_Radio) return;
                _comPort_Radio = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string ComPort_GSM
        {
            get => _comPort_GSM;
            set
            {
                if (value == _comPort_GSM) return;
                _comPort_GSM = value;
                OnPropertyChanged();
            }
        }


        [Range(5,100000)]
        public int CyclePool_Minutes
        {
            get => _cyclePool_Minutes;
            set
            {
                if (value == _cyclePool_Minutes) return;
                _cyclePool_Minutes = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsVisibleEN
        {
            get => en;
            set
            {
                if (en == value) return;
                en = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsVisibleRU
        {
            get => ru;
            set
            {
                if (ru == value) return;
                ru = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsVisibleAZ
        {
            get => az;
            set
            {
                if (az == value) return;
                az = value;
                OnPropertyChanged();
            }
        }


        public override Common Clone()
        {
            return new Common
            {
                Access = Access,
                Language = Language,
                CoordinateView = CoordinateView,
                ComPort_Radio = ComPort_Radio,
                ComPort_GSM = ComPort_GSM,
                CyclePool_Minutes = CyclePool_Minutes,
                IsVisibleAZ = IsVisibleAZ,
                IsVisibleEN = IsVisibleEN,
                IsVisibleRU = IsVisibleRU
            };
        }

        public override bool EqualTo(Common model)
        {
            return Language == model.Language
                && Access == model.Access
                && ComPort_GSM == model.ComPort_GSM
                && ComPort_Radio == model.ComPort_Radio
                && CoordinateView == model.CoordinateView
                && IsVisibleRU == model.IsVisibleRU
                && IsVisibleEN == model.IsVisibleEN
                && IsVisibleAZ == model.IsVisibleAZ
                && CyclePool_Minutes == model.CyclePool_Minutes;
        }

        public override void Update(Common model)
        {
            Access = model.Access;
            Language = model.Language;
            CoordinateView = model.CoordinateView;
            ComPort_Radio = model.ComPort_Radio;
            ComPort_GSM = model.ComPort_GSM;
            CyclePool_Minutes = model.CyclePool_Minutes;
            IsVisibleAZ = model.IsVisibleAZ;
            IsVisibleEN = model.IsVisibleEN;
            IsVisibleRU = model.IsVisibleRU;
        }
    }
}
