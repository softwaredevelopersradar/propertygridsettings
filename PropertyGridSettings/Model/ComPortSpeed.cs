﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyGridSettings.Model
{
    public class ComPortsSpeed : ObservableCollection<int>
    {
        public ComPortsSpeed()
        {
            Add(2400);
            Add(4800);
            Add(9600);
            Add(19200);
            Add(38400);
            Add(57600);
            Add(115200);
        }
    }
}
