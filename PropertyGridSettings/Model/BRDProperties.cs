﻿using PropertyGridSettings.Ext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;

namespace PropertyGridSettings.Model
{
    public class BRDProperties : AbstractBaseProperties<BRDProperties>
    {
        private bool existance = false;
        private string _comPort;
        private float _courseAngle = 0;
        private short _address = 1;
        private int portSpeed = 115200;

        public BRDProperties() { }

        [NotifyParentProperty(true)]
        public bool Availability
        {
            get => existance;
            set
            {
                if (existance == value) return;
                existance = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string ComPort
        {
            get => _comPort;
            set
            {
                if (value == _comPort) return;
                _comPort = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 359)]
        [NotifyParentProperty(true)]
        public float CourseAngle
        {
            get => _courseAngle;
            set
            {
                if (value == _courseAngle) return;
                _courseAngle = value;
                OnPropertyChanged();
            }
        }

        [Range(1, 24)]
        [NotifyParentProperty(true)]
        public short Address
        {
            get => _address;
            set
            {
                if (_address == value) return;
                _address = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public int PortSpeed
        {
            get => portSpeed;
            set
            {
                if (portSpeed == value) return;
                portSpeed = value;
                OnPropertyChanged();
            }
        }

        #region ModelMethods

        public override bool EqualTo(BRDProperties model)
        {
            return CourseAngle == model.CourseAngle
                && ComPort == model.ComPort
                && Availability == model.Availability
                && Address == model.Address
                && PortSpeed == model.PortSpeed;
        }

        public override void Update(BRDProperties model)
        {
            CourseAngle = model.CourseAngle;
            ComPort = model.ComPort;
            Availability = model.Availability;
            Address = model.Address;
            PortSpeed = model.PortSpeed;
        }

        public override BRDProperties Clone()
        {
            return new BRDProperties
            {
                Address = Address,
                CourseAngle = CourseAngle,
                ComPort = ComPort,
                Availability = Availability,
                PortSpeed = PortSpeed
            };
        }
        #endregion
    }
}
