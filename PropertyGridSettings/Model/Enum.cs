﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyGridSettings.Model
{
    public enum Languages : byte
    {
        [Description("Русский")]
        RU,
        [Description("English")]
        EN,
        [Description("Azərbaycan")]
        AZ
    }

    public enum ViewCoord : byte
    {
        [Description("DD.dddddd")]
        Dd = 1,
        [Description("DD MM.mmmm")]
        DMm = 2,
        [Description("DD MM SS.ss")]
        DMSs = 3
    }

    public enum AccessTypes : byte
    {
        Admin,
        User
    }
}
