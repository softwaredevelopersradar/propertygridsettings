﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Ports;
using System.Linq;

namespace PropertyGridSettings.Model
{
    public class ComPortsNames : ObservableCollection<string>
    {
        public ComPortsNames()
        {
            UpdatePort();
        }

        public void UpdatePort()
        {
            List<string> portnames = SerialPort.GetPortNames().ToList();
            if (portnames.Count == 0)
            {
                portnames.Add("COM1");
            }

            if (this.Equals(portnames))
                return;
            var except = portnames.Except(this).ToList();
            foreach (var name in except)
                Add(name);
            except = this.Except(portnames).ToList();
            foreach (var name in except)
                Remove(name);



        }
    }
}
