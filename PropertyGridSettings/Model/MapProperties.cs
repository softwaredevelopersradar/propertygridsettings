﻿using PropertyGridSettings.Ext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyGridSettings.Model
{
    public class MapProperties : AbstractBaseProperties<MapProperties>
    {
        private string imageASY = Environment.CurrentDirectory + "/Resources/Asy_1.png";
        private string imageStation = Environment.CurrentDirectory + "/Resources/Station_1.png";
        private string fileMap = "";
        private string folderMapTiles = "";
        private string fileDTEDElevation = "";
        private double currentScale;
        private double latitude = -1;
        private double longitude = -1;
        public string ImageASY
        {
            get => imageASY;
            set
            {
                if (imageASY == value) return;
                imageASY = value;
                OnPropertyChanged();
            }
        }

        public string ImageStation
        {
            get => imageStation;
            set
            {
                if (imageStation == value) return;
                imageStation = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string FileMap
        {
            get => fileMap;
            set
            {
                if (fileMap == value) return;
                fileMap = value;
                OnPropertyChanged();

            }
        }

        [NotifyParentProperty(true)]
        public string FolderMapTiles
        {
            get => folderMapTiles;
            set
            {
                if (folderMapTiles == value) return;
                folderMapTiles = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string FileDTEDElevation
        {
            get => fileDTEDElevation;
            set
            {
                if (fileDTEDElevation == value) return;
                fileDTEDElevation = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public double CurrentScale
        {
            get => currentScale;
            set
            {
                if (currentScale == value) return;
                currentScale = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public double Latitude
        {
            get => latitude;
            set
            {
                if (latitude == value) return;
                latitude = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public double Longitude
        {
            get => longitude;
            set
            {
                if (longitude == value) return;
                longitude = value;
                OnPropertyChanged();
            }
        }


        public override MapProperties Clone()
        {
            return new MapProperties
            {
                ImageASY = ImageASY,
                ImageStation = ImageStation,
                FileMap = FileMap,
                FolderMapTiles = FolderMapTiles,
                FileDTEDElevation = FileDTEDElevation,
                CurrentScale = CurrentScale,
                Latitude = Latitude,
                Longitude = Longitude
            };
        }

        public override bool EqualTo(MapProperties model)
        {
            return ImageStation == model.ImageStation
                && ImageASY == model.ImageASY
                && FileMap == model.FileMap
                && FolderMapTiles == model.FolderMapTiles
                && FileDTEDElevation == model.FileDTEDElevation
                && CurrentScale == model.CurrentScale
                && Latitude == model.Latitude
                && Longitude == model.Longitude;
        }

        public override void Update(MapProperties model)
        {
            ImageStation = model.ImageStation;
            ImageASY = model.ImageASY;
            FileMap = model.FileMap;
            FolderMapTiles = model.FolderMapTiles;
            FileDTEDElevation = model.FileDTEDElevation;
            CurrentScale = model.CurrentScale;
            Longitude = model.Longitude;
            Latitude = model.Latitude;
        }
    }
}
