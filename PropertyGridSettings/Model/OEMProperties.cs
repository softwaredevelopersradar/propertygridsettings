﻿using PropertyGridSettings.Ext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;

namespace PropertyGridSettings.Model
{
   public class OEMProperties : AbstractBaseProperties<OEMProperties>
   {

        private const string categoryLocal = "LocalPoint";
        private const string categoryRemoute = "RemotePoint";

        private string ipAddressLocal = "127.0.0.1";
        private int portLocal = 80;

        private string ipAddressRemoute = "127.0.0.1";
        private int portRemoute = 80;
        private bool existance = false;
        private float courseAngle = 0;

        public OEMProperties() { }

        [NotifyParentProperty(true)]
        public bool Availability
        {
            get => existance;
            set
            {
                if (existance == value) return;
                existance = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryLocal)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddressLocal
        {
            get => ipAddressLocal;
            set
            {
                if (ipAddressLocal == value) return;
                ipAddressLocal = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryLocal)]
        [Range(1, 1000000)]
        public int PortLocal
        {
            get => portLocal;
            set
            {
                if (portLocal == value) return;
                portLocal = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryRemoute)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddressRemoute
        {
            get => ipAddressRemoute;
            set
            {
                if (ipAddressRemoute == value) return;
                ipAddressRemoute = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryRemoute)]
        [Range(1, 1000000)]
        public int PortRemoute
        {
            get => portRemoute;
            set
            {
                if (portRemoute == value) return;
                portRemoute = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(0, 360)]
        public float CourseAngle
        {
            get => courseAngle;
            set
            {
                if (courseAngle == value) return;
                courseAngle = value;
                OnPropertyChanged();
            }
        }


        public override OEMProperties Clone()
        {
            return new OEMProperties
            {
                Availability = Availability,
                IpAddressLocal = IpAddressLocal,
                PortLocal = PortLocal,   
                PortRemoute = PortRemoute,
                IpAddressRemoute = IpAddressRemoute,
                CourseAngle = CourseAngle
            };
        }

        public override bool EqualTo(OEMProperties model)
        {
            return IpAddressLocal == model.IpAddressLocal
               && PortLocal == model.PortLocal
               && IpAddressRemoute == model.IpAddressRemoute
               && PortRemoute == model.PortRemoute
               && Availability == model.Availability
               && CourseAngle == model.CourseAngle;
        }

        public override void Update(OEMProperties model)
        {
            IpAddressLocal = model.IpAddressLocal;
            PortLocal = model.PortLocal;
            IpAddressRemoute = model.IpAddressRemoute;
            PortRemoute = model.PortRemoute;
            Availability = model.Availability;
            CourseAngle = model.CourseAngle;
        }
    }
}
